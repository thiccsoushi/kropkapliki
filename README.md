**preview:**

![img](preview.png?raw=true "preview")

### stuff i use:
* **os:** arch
* **wm:** dwm
* **status bar:** slstatus
* **shell:** zsh
* **terminal:** st/rxvt-unicode
* **file browser:** [joshuto](https://github.com/kamiyaa/joshuto)/ranger
* **text editor** nvim

### feel free to use my dotfiles for whatever it is that you desire
