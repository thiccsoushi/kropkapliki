if ! filereadable(expand('~/.config/nvim/autoload/plug.vim'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ~/.config/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ~/.config/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin('~/.vim/plugged')
Plug 'mboughaba/i3config.vim'
Plug 'ap/vim-css-color'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
"Plug 'Shougo/deoplete.nvim'
"Plug 'zchee/deoplete-clang'
Plug 'scrooloose/nerdtree'
Plug 'HerringtonDarkholme/yats.vim' " TS Syntax
Plug 'preservim/nerdcommenter'
Plug 'sheerun/vim-polyglot'
call plug#end()

set number relativenumber
set bg=dark
set nocompatible
set wildmenu
set mouse=a
set tabstop=4
set wildmode=longest,list,full
set clipboard+=unnamedplus

syntax on
filetype indent plugin on
colorscheme default

map <C-h> <C-w>h
map <C-l> <C-w>l

cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

map <C-n> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

"let g:deoplete#enable_at_startup = 1
